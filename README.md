# puppet-module-puppetmaster

Manages a Puppet Master

## Requirements
---

- Puppet version 3 or greater with Hiera support
- Puppet Modules:

| OS Family      | Module |
| :------------- |:-------------: |
| ALL            | [clabs/core](https://bitbucket.org/convectionlabs/puppet-module-core)|
| ALL            | [puppetlabs/inifile](https://forge.puppetlabs.com/puppetlabs/stdlib) |
| ALL            | [puppetlabs/stdlib](https://forge.puppetlabs.com/puppetlabs/inifile) |

## Usage
---

Loading the puppetmaster class:

```puppet
include puppetmaster
```

## Configuration
---

All configuration settings should be defined using Hiera.


### Puppet Master

Default settings (RedHat Linux):

```yaml
puppetmaster::enabled       : true
puppetmaster::svc           : 'puppetmaster'
puppetmaster::pkg           : 'puppet-server'
puppetmaster::version       : '3.4.3-1.el6'
puppetmaster::user          : 'puppet'
puppetmaster::group         : 'puppet'
puppetmaster::homedir       : '/var/lib/puppet'
puppetmaster::configfile    : '/etc/puppet/puppet.conf'
puppetmaster::packages      :
    - 'puppet-server'
    - 'hiera'
    - 'rubygem-deep_merge'
    - 'gcc'
    - 'ruby-devel'
puppetmaster::settings      :
    autosign        : true
    pluginsync      : true
    filetimeout     : 1
    dns_alt_names   : "puppet,%{::hostname}"
    modulepath      : '$confdir/environments/$environment/modules:$confdir/environments/$environment/dist'
    manifest        : '$confdir/environments/$environment/manifests/site.pp'
    environment     : %{::environment}
```

### Puppet R10k
Default settings:

```yaml
puppetmaster::r10k:
    enabled     : true
    usessh      : true
    autoupdate  : true
    updatecmd   : "r10k deploy environment %{::environment}"
    interval    : '*/1'
    configfile  : '/etc/r10k.yaml'
    cachedir    : '/var/cache/r10k'
    basedir     : '/etc/puppet/environments'
    remotes     : UNDEF
    ssh         : UNDEF
```

**Minimum Configuration**

- A remotes url and dir must be set. This should point to your GIT repository containing your custom puppet code, modules and hiera metadata.
- If **usessh** is enabled (default), a RSA based SSH public & private key for the ```puppetmaster::user``` must be set.
    This SSH keypair will be used by r10k to retrieve puppet code from your GIt repository set with the **remote** r10k setting.

Example Using:

- **github.com** git service provider
- a repository owner called **myusername**
- a private git repository called **puppet**
- an SSH RSA key used for authentication

**NOTE the use of a YAML string literal marker in ```'private': |``` and the indentation of the private key value. This is the required YAML format to preserve multi-line text.**

```yaml
puppetmaster::r10k:
    remotes: 
      dir: '/etc/puppet/environments'
      url: 'git@github.com:myusername/puppet.git'
    ssh:
        private: |
            -----BEGIN RSA PRIVATE KEY-----
            MIIEpQIBAAKCAQEAvUt32fzMVE9sPC0lSDsq/drwA5qa6rnZOeJvEQZDS9INeR8H
            Ofk/yyAPvZD+vBgBz8l+TILZSqrBB20vzcx05dc+NIug7K01UJA5OATVr7SISR/2
            Sr56d8rKnfjvnAYw0irTVOtx5c/xvPXkEPQcpNcq1Xb0yyesxWIqEjkAGHUS437s
            rbm0LRrmaA0iL2XmDXlvYky/botyPcB9LBsx6U630OvonxyHC3VOqEeGxaiysOhZ
            C04J53M1WvS66IANsAorUu4CHW8nXEPc3NzNY6dSfc92rNArSZWID1Eb/5tzM4Pr
            drnwljQ4Ev4WBwcpIWaDYUxlrBZqWIYh/tZ0qwIDAQABAoIBABnNQJ/o8l0HmmQ1
            byCCWCDUB+Ul3Lnj7TUTpTn31ZJvJkD9NrnzPHvOefbH3cD7sLuG5n09VKr1zWva
            uSpfFQwDI8p7wcqg7WR21CXLmujhPbZh8etMPVs4vBd+SALHYT40civgD91YU8YC
            6jxZWdIqNf90wHc0iDs7XyqutFGg32sKwUJ2+4crHJ+yNst1vsAT/vk8YP3r9RWv
            YJiiRv/W75jn16wK94svcIUci/x1UzJj2GPAIhTFHnBxMEhMYDfGQ2+cmCEFFTIp
            S3WhUrvdea6/cIwQme9T7NCLbhPM9dXierjQ1G31a8nJhMK2DDLEDWnGTeXQp9bO
            jSaVGVECgYEA7a0RlvummzjuBBvOpsKRcVGqqt98lJ8ByM6UwvVWcvZQdZba9UK0
            fDo+KTVYmYLjKRJfKTYRbls1iBaxw87tZV1TGWSDoBjKjpv3u/oMMi9zWLr2eiew
            hKk66AIdOibhGjJX1vpJrhBY2gWLFQ26eTxy0zDdzBMC1/cB9HDxaEkCgYEAy+OD
            4TlcbtY8/uAJc38KcPSzXoo5tRNt8xhTEQSe/EYEQ6oMWUzMWzoEdYgcCAbs3a5E
            jsmsNLKi+eGpKrLYMfZPAfGf1ks4lE1dl4gIgdYdD/bqUN/DkswW/R5clkxNvr6B
            Uc4nmveEmU+VJe+PuGHkheyFa8VO50CFH/d2fVMCgYEA2tBrUUV9rYpMFxxCtOSM
            Ynd9WLNITsvVKLqH2rVkUZLToG6yRvqSj4xPjkC+wRXvbPCtC5PrmF4AernqDwuD
            I783sjkHtW0djw2oljcu40UsPJRkNzIzIO9sfAz2O4Nv2l5e4FytWMNUCeTwlwF8
            nAE69ZQXnroh9BJEincwyYECgYEAmBvDnrqJkBL67Hc99dF4e67DRqu+w62cIHks
            N40IA1NFCSoLDnAyIXkWLIz/xKlN+1Bwy0n/1EzVM7E74KQOkK+eX+fghCIYMuPJ
            CCyvlBWmFuvyAaX+pTznha0qC+MfOUes+NfhSkaQwUPTFLs1sKwvrg43t6A67mF/
            L8sRtgsCgYEAgGv/UwTusOGHkw6Rd0DoF0PDrsh4uahfiATN7hlpLfPpOLLD8fNM
            O1jZgR4DhtW7Yjc4210xPjCZT6HXxJNd2/bP6H676y1tqCr+Dn89Ra0Exrc58JDF
            Is2lXRdCUieVeQRNi6z6mQ7pvch5faxrl6xpXNhdHBsCr3UQrBWy3v4=
            -----END RSA PRIVATE KEY-----
        public: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9S3fZ/MxUT2w8LSVIOyr92vADmprqudk54m8RBkNL0g15Hwc5+T/LIA+9kP68GAHPyX5MgtlKqsEHbS/NzHTl1z40i6DsrTVQkDk4BNWvtIhJH/ZKvnp3ysqd+O+cBjDSKtNU63Hlz/G89eQQ9Byk1yrVdvTLJ6zFYioSOQAYdRLjfuytubQtGuZoDSIvZeYNeW9iTL9ui3I9wH0sGzHpTrfQ6+ifHIcLdU6oR4bFqLKw6FkLTgnnczVa9LrogA2wCitS7gIdbydcQ9zc3M1jp1J9z3as0CtJlYgPURv/m3Mzg+t2ufCWNDgS/hYHBykhZoNhTGWsFmpYhiH+1nSr mykeyalias'
```

### Puppet Hiera

Default settings:

```yaml
puppetmaster::hiera:
    configfile: '/etc/puppet/hiera.yaml'
```

## See Also

* [Configuring Puppet](http://docs.puppetlabs.com/guides/configuring.html)
* [Hiera Overview](http://docs.puppetlabs.com/hiera/1/)
* [r10k - Puppet Environment & Module Deployment](https://github.com/adrienthebo/r10k)

