# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 foldmethod=marker smartindent
#
# == Define: puppetmaster::config::unset
# ---
#
# Unsets a Puppet Master configuration setting
#
# NOTE: internal use only
#
# === Parameters
# ---
#
# [*name*]
# - Type - string
# - Puppet master configuration setting
#
define puppetmaster::config::unset() {

  ensure_resource('ini_setting', "puppet-master-${name}",
    {
      'setting' => $name,
      'ensure'  => 'absent',
      'path'    => "${::puppetmaster::configdir}/puppet.conf",
      'section' => 'master',
      'notify'  => Class['::puppetmaster::service'],
    }
  )

}
