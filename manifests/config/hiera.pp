# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 foldmethod=marker smartindent
#
# == Class: puppetmaster::config::hiera
# ---
#
# Manages the Puppet Master Hiera configuration
#
# === Parameters
# ---
#
# [*defaults*]
# - NOTE - Use puppetmaster::hiera via Hiera to override any defaults
# - Type - Hash
# - Default
#   {
#     'configfile'  => '/etc/puppet/hiera.yaml',
#   }
#
#
class puppetmaster::config::hiera(

  $defaults = {
    'configfile'  => "${::puppetmaster::configdir}/hiera.yaml",
  },

  $user     = $::puppetmaster::user,
  $hiera    = $::puppetmaster::hiera,

) inherits puppetmaster {

  $mergedsettings = merge($defaults, $::puppetmaster::r10k, $::puppetmaster::hiera)

  $configfile   = $mergedsettings['configfile']
  $datasources  = $mergedsettings['datasources']
  $remotes      = $mergedsettings['remotes']

  validate_absolute_path  ( $configfile  )
  validate_array          ( $datasources )
  validate_hash           ( $remotes     )

  clabs::template {
    $configfile:
      owner   => $user,
      group   => 'adm',
      mode    => '0440',  # secure (may contain passwords)
      notify  => Class['::puppetmaster::service'],
  }

  clabs::link {
    '/etc/hiera.yaml':
      target  => $mergedsettings['configfile'],
      force   => true,
  }

}
