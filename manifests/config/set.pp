# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 foldmethod=marker smartindent
#
# == Define: puppetmaster::config::set
# ---
#
# Sets a Puppet Master configuration setting
#
# NOTE: internal use only
#
# === Parameters
# ---
#
# [*name*]
# - Type - string
# - Puppet master configuration setting
#
define puppetmaster::config::set() {

  $value = $::puppetmaster::config::mergedsettings[$name]

  ensure_resource('ini_setting', "puppet-master-${name}",
    {
      'setting' => $name,
      'value'   => $value,
      'ensure'  => 'present',
      'path'    => "${::puppetmaster::configdir}/puppet.conf",
      'section' => 'master',
      'notify'  => Class['::puppetmaster::service'],
    }
  )

}
