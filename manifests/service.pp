# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 foldmethod=marker smartindent
#
# == Class: puppetmaster::service
# ---
#
# Manages the Puppet Master service
#
# NOTE: internal use only
#
class puppetmaster::service(

  $enabled    = $::puppetmaster::passenger ? {
    false   => $::puppetmaster::enabled,
    default => false,
  },
  $svc        = $::puppetmaster::svc,

) inherits puppetmaster {

  service {
    $svc:
      ensure    => $enabled,
      enable    => $enabled,
  }

}

