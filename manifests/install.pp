# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 foldmethod=marker smartindent
#
# == Class: puppetmaster::install
# ---
#
# Installs the Puppet Master
#
class puppetmaster::install(

  $pkg      = $::puppetmaster::pkg,
  $version  = $::puppetmaster::version,

) {

  ensure_packages( $pkg, {
    ensure => $version,
  })

}

