# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 foldmethod=marker smartindent
#
# == Class: puppetmaster::config
# ---
#
# - Configures the Puppet Master
# - Loads puppetmaster::config::hiera
# - Loads puppetmaster::config::r10k
#
# === Parameters
# ---
#
# [*defaults*]
# - NOTE - Use puppetmaster::settings via Hiera to override any defaults
# - Type - Hash
#
class puppetmaster::config(

  $defaults   = $::puppetmaster::defaults,
  $settings   = $::puppetmaster::settings,
  $configfile = "${::puppetmaster::configdir}/puppet.conf",
  $user       = $::puppetmaster::user,
  $group      = $::puppetmaster::group,
  $passenger  = $::puppetmaster::passenger,

) inherits puppetmaster {

  contain "${name}::hiera"

  if $passenger {
    contain "${name}::passenger"
  }

  if $r10k {
    contain "${name}::r10k"
  }

  # -- Puppet Master Configuration --

  $mergedsettings = merge($defaults, $settings)

  validate_bool   ( $mergedsettings['pluginsync']     )
  validate_bool   ( $mergedsettings['autosign']       )
  validate_string ( $mergedsettings['dns_alt_names']  )
  validate_string ( $mergedsettings['environment']    )

  if type($mergedsettings['filetimeout']) != 'integer' { fail('filetimeout is not an integer') }

  #if versioncmp($::puppetversion,'3.6') == -1 {
    validate_string ( $mergedsettings['modulepath']   )
    validate_string ( $mergedsettings['manifest']     )

  # Remove legacy puppet settings
  #} else {
  #ensure_resource("${name}::unset", 'modulepath'  )
  #ensure_resource("${name}::unset", 'manifest'    )
  #ensure_resource("${name}::unset", 'manifestdir' )
  #}

  $mergedkeys = keys($mergedsettings)

  ensure_resource("${name}::set", $mergedkeys)

  # Set file perms & ownership
  ensure_resource('file', $configfile, {
    owner => $user,
    group => $group,
    mode  => '0660', # secure (may contain passwords)
  })

}

