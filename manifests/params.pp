# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 foldmethod=marker smartindent
#
# == Class: puppetmaster::params
# ---
#
# - Provides Puppet Master parameter defaults
#
class puppetmaster::params {

  # $defaults

  # Legacy puppet
  if versioncmp($::puppetversion,'3.6') == -1 {
    $defaults = {
      'autosign'      => true,
      'pluginsync'    => true,
      'filetimeout'   => 1,
      'dns_alt_names' => "puppet,${::hostname}",
      'modulepath'    => '$confdir/environments/$environment/modules:$confdir/environments/$environment/dist',
      'manifest'      => '$confdir/environments/$environment/manifests',
      'environment'   => $::environment,
    }

  # Puppet 3.6+ (directory environments support)
  } else {
    $defaults = {
      'autosign'        => true,
      'pluginsync'      => true,
      'filetimeout'     => 1,
      'dns_alt_names'   => "puppet,${::hostname}",
      'environmentpath' => '$confdir/environments',
      'environment'     => $::environment,
    }
  }

  # NOTE: hiera_hash does not work as expected in a parameterized class
  #   definition; so we call it here.
  #
  # http://docs.puppetlabs.com/hiera/1/puppet.html#limitations
  # https://tickets.puppetlabs.com/browse/HI-118
  #
  $settings = hiera_hash('puppetmaster::settings',      {})
  $r10k     = hiera_hash('puppetmaster::r10k',          {})
  $remotes  = hiera_hash('puppetmaster::r10k::remotes', {})
  $hiera    = hiera_hash('puppetmaster::hiera',         {})

  $passenger = false

}

