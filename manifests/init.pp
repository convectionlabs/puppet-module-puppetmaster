# vim: tabstop=2 expandtab shiftwidth=2 softtabstop=2 foldmethod=marker smartindent
#
# == Class: puppetmaster
# ---
#
# Manages the Puppet Master
#
# === Parameters
# ---
#
# [*enabled*]
# - Type - Boolean
# - Default - true
# - Enable/disable the service
#
# [*svc*]
# - Type - String
# - Default - puppetmaster
# - Master service name
#
# [*pkg*]
# - Type - String
# - Default - puppet-server
# - Master package name
#
# [*version*]
# - Type - String
# - Default - 3.4.3 (OS specific)
# - Puppet Master Version
#
# [*user*]
# - Type - String
# - Default - puppet
# - Master service run-as user
#
# [*group*]
# - Type - String
# - Default - puppet
# - Master service run-as group
#
# [*vardir*]
# - Type - String
# - Default - /var/lib/puppet
# - Master service run time directory
#
# [*packages*]
# - Type - Array
# - Default
#   [
#    'hiera',
#    'rubygem-deep_merge',
#    'gcc',
#    'ruby-devel',
#   ]
# - Supporting package installation list
#
# [*configdir*]
# - Type - String
# - Default - OS Specific
# - Master configuration files location (absolute path)
#
# [*settings*]
# - Type - Hash
# - Default - {}
# - Master puppet configuration file settings
#
class puppetmaster (

  $enabled    = true,
  $svc        = 'puppetmaster',
  $pkg        = 'puppet-server',
  $version    = $::osfamily ? {
    'RedHat'  => '3.4.3-1.el6',
    default   => undef,
  },
  $user       = 'puppet',
  $group      = 'puppet',
  $vardir     = '/var/lib/puppet',
  $configdir  = '/etc/puppet',
  $packages   = [
    # Hiera
    'hiera',
    'rubygem-deep_merge',
    # r10k
    'gcc',
    'ruby-devel',
  ],

  $defaults   = $::puppetmaster::params::defaults,
  $settings   = $::puppetmaster::params::settings,
  $r10k       = $::puppetmaster::params::r10k,
  $hiera      = $::puppetmaster::params::hiera,
  $passenger  = $::puppetmaster::params::passenger,
  $remotes    = $::puppetmaster::params::remotes,

) inherits puppetmaster::params {

  validate_bool           ( $enabled    )
  validate_string         ( $svc        )
  validate_string         ( $pkg        )
  validate_string         ( $version    )
  validate_string         ( $user       )
  validate_string         ( $group      )
  validate_absolute_path  ( $vardir     )
  validate_absolute_path  ( $configdir  )
  validate_array          ( $packages   )
  validate_hash           ( $defaults   )
  validate_hash           ( $settings   )
  validate_hash           ( $r10k       )
  validate_hash           ( $hiera      )
  validate_hash           ( $remotes    )

  clabs::module::init { $name: }

}

